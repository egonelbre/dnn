package main

import (
	"bufio"
	"flag"
	"fmt"
	"math/rand"
	"os"

	"gitlab.com/egonelbre/dnn/codegen"
	"gitlab.com/egonelbre/dnn/graph"
	"gitlab.com/egonelbre/dnn/ssa"
	"gitlab.com/egonelbre/dnn/tensor"
)

func Load(t *tensor.Tensor, name string) {
	for it := t.Iterate(nil); it.Next(); {
		t.Set(it.Location, ssa.NamedLoad(name, it.Location))
	}
}

func Store(t *tensor.Tensor, name string) {
	for it := t.Iterate(nil); it.Next(); {
		t.Set(it.Location, ssa.NamedStore(name, it.Location, t.Get(it.Location)))
	}
}

func RandomConst(t *tensor.Tensor, n int) {
	for it := t.Iterate(nil); it.Next(); {
		v := float64(rand.Intn(n))
		t.Set(it.Location, ssa.Const(v))
	}
}

var (
	multiplyAdd = flag.Bool("multiply-add", false, "")
	cseSlow     = flag.Bool("cse-slow", false, "")
	group       = flag.Int("group", 1<<20, "")
)

func main() {
	flag.Parse()
	rand.Seed(0)

	block := tensor.New(6, 6, 3)
	Load(block, "s")

	//  width, height, input channels, output channels
	kernel := tensor.New(3, 3, 3, 64)
	RandomConst(kernel, 8)

	convolved := block.Convolve(kernel, []int{1, 1, 0})
	pooled := convolved.Pool("Max", []int{2, 2, 1}, []int{2, 2, 1})
	Store(pooled, "d")

	fmt.Println(convolved)
	fmt.Println(pooled)

	list := []*ssa.Expr{}

	fmt.Println("----")
	for i := 0; i < len(pooled.Data); i += *group {
		last := i + *group
		if last > len(pooled.Data) {
			last = len(pooled.Data)
		}
		fmt.Println("--")
		list = append(list,
			Optimize(ssa.Clone(pooled.Data[i:last]))...,
		)
	}
	fmt.Println("----")

	totalStats := ssa.CalculateStats(list)
	fmt.Println("Shape:", block.Shape, "->", convolved.Shape, "->", pooled.Shape)
	fmt.Println("Total:", totalStats)

	{
		fmt.Println("writing cpp")
		os.Mkdir("build", 0755)
		w, _ := os.Create("build/kernel.cpp")
		bw := bufio.NewWriter(w)
		codegen.Cpp(w, list)
		bw.Flush()
		w.Close()
	}

	{
		fmt.Println("writing graphml")
		w, _ := os.Create("graph.graphml")
		bw := bufio.NewWriter(w)
		graph.WriteGraphML(bw, list)
		bw.Flush()
		w.Close()
	}

	{
		fmt.Println("writing svg")
		w, _ := os.Create("graph.svg")
		bw := bufio.NewWriter(w)
		graph.WriteMatrixSVG(bw, list)
		bw.Flush()
		w.Close()
	}
}

func Optimize(roots []*ssa.Expr) []*ssa.Expr {
	passes := []ssa.Pass{
		// unify duplicate expressions
		&ssa.InternSort{},
		// basic math optimizations
		&ssa.MultiplyZero{},
		&ssa.AddZero{},
		&ssa.MultiplyOne{},
		&ssa.InternSort{},
	}

	if *multiplyAdd {
		passes = append(passes,
			&ssa.MultiplyAdd{},
			&ssa.InternSort{},
		)
	}

	if *cseSlow {
		passes = append(passes,
			// multiply add
			// &ssa.CSE6{},
			// &ssa.InternSort{},
			//&ssa.CSE5{},
			//&ssa.InternSort{},
			&ssa.CSE4{},
			&ssa.InternSort{},
		)
	}

	passes = append(passes,
		&ssa.CSE2{},
		&ssa.InternSort{},
		&ssa.CSE2{},
		&ssa.InternSort{},
		&ssa.Order{},
		&ssa.InternSort{},
	)

	before := ssa.Flatten(roots, nil)
	beforeStats := ssa.CalculateStats(before)
	startStats := beforeStats
	fmt.Println(beforeStats)
	for _, pass := range passes {
		after := pass.Run(before)
		afterStats := ssa.CalculateStats(after)
		fmt.Printf("%-15s %5v => %5v\t%v\t%+v\n", pass.Name(), len(before), len(after), afterStats.Sub(beforeStats), pass)
		before = after
		beforeStats = afterStats
	}
	fmt.Println("Impro:", beforeStats.Sub(startStats))
	fmt.Println("Final:", beforeStats)

	return before
}
