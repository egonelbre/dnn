package graph

import (
	"fmt"
	"io"

	"github.com/loov/layout"
	"github.com/loov/layout/format/graphml"
	"gitlab.com/egonelbre/dnn/ssa"
)

func WriteGraphML(w io.Writer, exprs []*ssa.Expr) error {
	return graphml.Write(w, layoutGraph(exprs))
}

func layoutGraph(exprs []*ssa.Expr) *layout.Graph {
	list := ssa.Flatten(exprs, nil)

	g := layout.NewGraph()

	id := func(e *ssa.Expr) string {
		return fmt.Sprintf("%p", e)
	}

	for _, expr := range list {
		if expr.IsConst() {
			continue
		}
		g.Node(id(expr)).Label = expr.Label()
	}

	writeLink := func(source, target *ssa.Expr) {
		if source == nil || target == nil || source.IsConst() {
			return
		}
		g.Edge(id(source), id(target))
	}

	for _, target := range list {
		for _, source := range target.CallArgs {
			writeLink(source, target)
		}
		writeLink(target.Value, target)
	}

	return g
}
