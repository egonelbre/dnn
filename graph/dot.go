package graph

import (
	"fmt"
	"io"

	"gitlab.com/egonelbre/dnn/ssa"
)

func WriteDOT(w io.Writer, exprs []*ssa.Expr) error {
	list := ssa.Flatten(exprs, nil)

	write := func(format string, args ...interface{}) {
		fmt.Fprintf(w, format+"\n", args...)
		// TODO: handle errors
	}

	write(`digraph G {
	rankdir=LR;
`)
	defer write("}")

	for _, expr := range list {
		if expr.IsConst() {
			continue
		}
		write("n%p [shape=box; label=\"%s\"];", expr, expr.Label())

	}

	writeLink := func(source, target *ssa.Expr) {
		if source == nil || target == nil || source.IsConst() {
			return
		}
		write("n%p -> n%p;", source, target)
	}

	for _, target := range list {
		for _, source := range target.CallArgs {
			writeLink(source, target)
		}
		writeLink(target.Value, target)
	}

	return nil
}
