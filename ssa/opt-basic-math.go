package ssa

type MultiplyZero struct {
	Deleted int
}

func (MultiplyZero) Name() string { return "MultiplyZero" }

// MultiplyZero a * 0 => 0
func (s *MultiplyZero) Run(list []*Expr) []*Expr {
	s.Deleted = 0
	for _, e := range list {
		if e.Kind == KindCall && e.Name == "Mul" {
			for _, arg := range e.CallArgs {
				if arg.Kind == KindConst && arg.Const == 0 {
					e.Kind = KindConst
					e.Const = 0
					s.Deleted++
					break
				}
			}
		}
	}
	return trimZero(list)
}

type MultiplyOne struct {
	Eliminated int
	Deleted    int
}

func (MultiplyOne) Name() string { return "MultiplyOne" }

// MultiplyOne a * 0 => 0
func (s *MultiplyOne) Run(list []*Expr) []*Expr {
	for _, e := range list {
		if e.Kind == KindCall && e.Name == "Mul" {
			if len(e.CallArgs) == 0 {
				panic("invalid mul")
			}
			for i := len(e.CallArgs) - 1; i >= 0; i-- {
				arg := e.CallArgs[i]
				if arg.Kind == KindConst && arg.Const == 1 {
					e.CallArgs = append(e.CallArgs[:i], e.CallArgs[i+1:]...)
					s.Eliminated++
				}
			}
			if len(e.CallArgs) == 1 {
				e.Kind = KindRef
				e.Value = e.CallArgs[0]
				e.CallArgs = nil
				s.Deleted++
			}
		}
	}
	return flattenRef(list)
}

type AddZero struct {
	Eliminated int
	Deleted    int
}

func (AddZero) Name() string { return "AddZero" }

// AddZero a + 0 + c => a + c
func (s *AddZero) Run(list []*Expr) []*Expr {
	for _, e := range list {
		if e.Kind == KindCall && e.Name == "Add" {
			for i := len(e.CallArgs) - 1; i >= 0; i-- {
				arg := e.CallArgs[i]
				if arg.Kind == KindConst && arg.Const == 0 {
					e.CallArgs = append(e.CallArgs[:i], e.CallArgs[i+1:]...)
					s.Eliminated++
				}
			}
			if len(e.CallArgs) == 0 {
				e.Kind = KindConst
				e.Const = 0
				s.Deleted++
			}
		}
	}
	return trimZero(list)
}

func trimZero(list []*Expr) []*Expr {
	r := list[:0]
	for _, expr := range list {
		if expr.Kind == KindConst && expr.Const == 0 {
			continue
		}
		r = append(r, expr)
	}
	return r
}

func flattenRef(list []*Expr) []*Expr {
	r := list[:0]

	var deref func(e *Expr) *Expr
	deref = func(e *Expr) *Expr {
		if e == nil {
			return nil
		}
		if e.Kind == KindRef {
			return deref(e.Value)
		}
		return e
	}

	for _, expr := range list {
		if expr.Kind == KindRef {
			continue
		}
		expr.Value = deref(expr.Value)
		for i, arg := range expr.CallArgs {
			expr.CallArgs[i] = deref(arg)
		}
		r = append(r, expr)
	}
	return r
}
