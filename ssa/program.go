package ssa

type Program struct {
	Roots []*Expr
	List  []*Expr
}

func NewProgram() *Program {
	return &Program{}
}

func (p *Program) AddRoot(roots ...*Expr) {
	p.Roots = append(p.Roots, roots...)
}
