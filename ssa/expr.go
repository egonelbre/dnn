package ssa

import (
	"fmt"
	"strconv"
	"strings"
)

type Expr struct {
	Kind Kind

	// KindConst
	Const float64
	// KindLoad, KindStore
	Location []int
	Value    *Expr

	// KindCall
	Name      string
	CallFlags FuncFlag
	CallArgs  []*Expr
}

type Kind byte

const (
	KindConst Kind = iota
	KindName
	KindRef
	KindLoad
	KindStore
	KindCall
)

type FuncFlag byte

const (
	FuncCommutative FuncFlag = 1 << iota
	FuncAssociative

	FuncCall   = 0
	FuncBinary = FuncCommutative | FuncAssociative
)

func (e *Expr) IsConst() bool {
	return e.Kind == KindConst || e.Kind == KindName
}

func (e *Expr) IsBinary() bool {
	return e.Kind == KindCall && e.CallFlags == FuncBinary
}

func (e *Expr) LocalCost() int {
	if e.Kind == KindConst {
		return 0
	} else if e.Kind == KindCall {
		// todo: add common costs
		return len(e.CallArgs) - 1
	} else {
		return 1
	}
}

func (e *Expr) Eq(b *Expr) bool {
	if e == b {
		return true
	}

	if e == nil || b == nil {
		return e == b
	}

	if e.Kind != b.Kind ||
		e.Const != b.Const ||
		e.Name != b.Name ||
		e.CallFlags != b.CallFlags ||
		len(e.Location) != len(b.Location) ||
		len(e.CallArgs) != len(b.CallArgs) {
		return false
	}

	for i := range e.Location {
		if e.Location[i] != b.Location[i] {
			return false
		}
	}
	if !e.Value.Eq(b.Value) {
		return false
	}

	for i := range e.CallArgs {
		if !e.CallArgs[i].Eq(b.CallArgs[i]) {
			return false
		}
	}

	return true
}

func (expr *Expr) IndexOfArg(e *Expr) int {
	for i, arg := range expr.CallArgs {
		if arg == e {
			return i
		}
	}
	return -1
}

func (expr *Expr) HasArg(e *Expr) bool {
	return expr.IndexOfArg(e) >= 0
}

func (expr *Expr) HasArgs(xs []*Expr) bool {
	for _, e := range xs {
		if !expr.HasArg(e) {
			return false
		}
	}
	return true
}

func (expr *Expr) RemoveArg(e *Expr) bool {
	i := expr.IndexOfArg(e)
	if i >= 0 {
		expr.CallArgs = append(expr.CallArgs[:i], expr.CallArgs[i+1:]...)
	}
	return i >= 0
}

func (e *Expr) ShallowClone() *Expr {
	if e == nil {
		return nil
	}

	clone := *e
	clone.Location = append(clone.Location[:0], clone.Location...)
	clone.CallArgs = append(clone.CallArgs[:0], clone.CallArgs...)

	return &clone
}

func (expr *Expr) Label() string {
	switch expr.Kind {
	case KindConst:
		return fmt.Sprintf("%v", expr.Const)
	case KindName:
		return expr.Name
	case KindRef:
		return "@" + expr.Value.Label()
	case KindLoad:
		return fmt.Sprintf("%v%v", expr.Name, expr.Location)
	case KindStore:
		return fmt.Sprintf("%v%v", expr.Name, expr.Location)
	case KindCall:
		var xs []string
		for _, arg := range expr.CallArgs {
			if arg.IsConst() {
				xs = append(xs, arg.Label())
			}
		}
		if op := Operators[expr.Name]; op != "" {
			if len(xs) > 0 {
				return op + " " + strings.Join(xs, " "+op+" ")
			}
			return op
		}
		if len(xs) > 0 {
			return expr.Name + "(" + strings.Join(xs, ",") + ")"
		}
		return expr.Name
	}
	return "???"
}

func (expr *Expr) Normalized() string {
	var b strings.Builder
	var write func(arg *Expr)
	write = func(arg *Expr) {
		switch arg.Kind {
		case KindConst:
			b.WriteString(fmt.Sprintf("%v", arg.Const))
		case KindName:
			b.WriteString(arg.Name)
		case KindRef:
			b.WriteString("@")
			write(expr.Value)
		case KindLoad:
			s := arg.Name
			if s != "" {
				s += ", "
			}
			b.WriteString(fmt.Sprintf("Load(%v%v)", s, arg.Location))
		case KindStore:
			b.WriteString(fmt.Sprintf("Store(%v, %v)", arg.Location, arg.Value))
		case KindCall:
			b.WriteString(arg.Name)
			b.WriteString("(")
			for i, child := range arg.CallArgs {
				if i != 0 {
					b.WriteString(", ")
				}
				write(child)
			}
			b.WriteString(")")
		}
	}
	write(expr)

	return b.String()
}

func (expr *Expr) ShortString() string {
	var b strings.Builder

	writeLocation := func(loc []int) {
		b.WriteString("[")
		for i, coord := range loc {
			if i != 0 {
				b.WriteString(",")
			}
			b.WriteString(strconv.Itoa(coord))
		}
		b.WriteString("]")
	}

	var write func(arg *Expr)
	write = func(arg *Expr) {
		switch arg.Kind {
		case KindConst:
			b.WriteString(fmt.Sprintf("%v", arg.Const))
		case KindName:
			b.WriteString(arg.Name)
		case KindRef:
			b.WriteString("@")
			write(expr.Value)
		case KindLoad:
			b.WriteString(arg.Name)
			writeLocation(arg.Location)
		case KindStore:
			b.WriteString(arg.Name)
			writeLocation(arg.Location)
			b.WriteString(" := ")
			write(arg.Value)
		case KindCall:
			if op, isOperator := Operators[arg.Name]; isOperator {
				b.WriteString("(")
				for i, child := range arg.CallArgs {
					if i != 0 {
						b.WriteString(" " + op + " ")
					}
					write(child)
				}
				b.WriteString(")")
			} else {
				b.WriteString(arg.Name)
				b.WriteString("(")
				for i, child := range arg.CallArgs {
					if i != 0 {
						b.WriteString(", ")
					}
					write(child)
				}
				b.WriteString(")")
			}
		}
	}
	write(expr)

	return b.String()
}

func (expr *Expr) String() string { return expr.ShortString() }
