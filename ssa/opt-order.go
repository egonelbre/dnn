package ssa

type Order struct{}

func (Order) Name() string { return "Order" }

func (s *Order) Run(list []*Expr) []*Expr {
	refCount := make(map[*Expr]int)
	for _, expr := range FlattenNonConst(list) {
		for _, arg := range expr.CallArgs {
			refCount[arg]++
		}
	}

	list = Flatten(list, func(expr *Expr) bool {
		return (!expr.IsConst() && (refCount[expr] > 1)) || (expr.Kind == KindStore)
	})

	var collectDependencies func(expr *Expr, into *[]*Expr)
	collectDependencies = func(expr *Expr, into *[]*Expr) {
		if expr == nil || expr.IsConst() {
			return
		}

		if refCount[expr] >= 2 {
			*into = append(*into, expr)
		} else {
			collectDependencies(expr.Value, into)
			for _, arg := range expr.CallArgs {
				collectDependencies(arg, into)
			}
		}
	}

	depends := make(map[*Expr][]*Expr, len(list))
	for _, expr := range list {
		list := []*Expr{}

		collectDependencies(expr.Value, &list)
		for _, arg := range expr.CallArgs {
			collectDependencies(arg, &list)
		}

		sortArgs(list)
		list = removeDupsFromSorted(list)

		depends[expr] = list
	}

	return list
}
