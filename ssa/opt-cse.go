package ssa

type CSE2 struct {
	Eliminated int
}

func (CSE2) Name() string { return "CSE2" }

// CSE2 q = a + x + b; w = a + b  => w = a + b; q = a + w
func (s *CSE2) Run(list []*Expr) []*Expr {
	table := NewTable()
	table.HashAll(list)

	type Partial struct {
		Func  string
		Left  *Expr
		Right *Expr
	}

	uncollapsed := Flatten(list, func(e *Expr) bool {
		return e.Kind == KindCall && e.CallFlags == FuncBinary
	})

	nextuncollapsed := []*Expr{}

	for len(uncollapsed) > 0 {
		count := map[Partial]int{}
		for _, expr := range uncollapsed {
			if expr.Kind == KindCall {
				for i, left := range expr.CallArgs {
					for _, right := range expr.CallArgs[i+1:] {
						count[Partial{expr.Name, left, right}]++
					}
				}
			}
		}

		for _, expr := range uncollapsed {
			if len(expr.CallArgs) <= 2 {
				continue
			}

			highest := 0
			var best Partial
			var bestLeft, bestRight int

			for i, left := range expr.CallArgs {
				for k, right := range expr.CallArgs[i+1:] {
					key := Partial{expr.Name, left, right}
					if instances := count[key]; instances > highest {
						highest = instances
						best = key
						bestLeft, bestRight = i, i+1+k
					}
				}
			}

			if highest <= 1 {
				continue
			}

			s.Eliminated++

			copy(expr.CallArgs[bestRight:], expr.CallArgs[bestRight+1:])
			copy(expr.CallArgs[bestLeft:], expr.CallArgs[bestLeft+1:])
			expr.CallArgs = expr.CallArgs[:len(expr.CallArgs)-2]

			lift := table.Intern(&Expr{
				Kind:      KindCall,
				Name:      expr.Name,
				CallFlags: expr.CallFlags,
				CallArgs:  []*Expr{best.Left, best.Right},
			})

			for _, left := range expr.CallArgs {
				list := []*Expr{left, lift}
				sortArgs(list)
				key := Partial{expr.Name, list[0], list[1]}
				count[key]++
			}

			expr.CallArgs = append(expr.CallArgs, lift)
			nextuncollapsed = append(nextuncollapsed, expr)
		}

		uncollapsed, nextuncollapsed = nextuncollapsed, uncollapsed[:0]
	}

	return RemoveUnused(list)
}

type CSE3 struct {
	Eliminated int
}

func (CSE3) Name() string { return "CSE3" }

// CSE3 q = a + c + x + b; w = a + c + b  => w = a + b + c; q = a + w
func (s *CSE3) Run(list []*Expr) []*Expr {
	table := NewTable()
	table.HashAll(list)

	type Partial struct {
		Func   string
		Left   *Expr
		Center *Expr
		Right  *Expr
	}

	uncollapsed := Flatten(list, func(e *Expr) bool {
		return e.Kind == KindCall && e.CallFlags == FuncBinary
	})

	nextuncollapsed := []*Expr{}

	for len(uncollapsed) > 0 {

		count := map[Partial]int{}
		for _, expr := range uncollapsed {
			if expr.Kind == KindCall {
				for i, left := range expr.CallArgs {
					for j, center := range expr.CallArgs[i+1:] {
						nj := i + 1 + j
						for _, right := range expr.CallArgs[nj:] {
							count[Partial{expr.Name, left, center, right}]++
						}
					}

				}
			}
		}

		for _, expr := range uncollapsed {
			if len(expr.CallArgs) <= 3 {
				continue
			}

			highest := 0
			var best Partial
			var bestLeft, bestCenter, bestRight int

			for i, left := range expr.CallArgs {
				for j, center := range expr.CallArgs[i+1:] {
					nj := i + 1 + j
					for k, right := range expr.CallArgs[nj+1:] {
						nk := nj + 1 + k
						key := Partial{expr.Name, left, center, right}
						if instances := count[key]; instances > highest {
							highest = instances
							best = key
							bestLeft, bestCenter, bestRight = i, nj, nk
						}
					}
				}
			}

			if highest <= 1 {
				continue
			}

			s.Eliminated++

			copy(expr.CallArgs[bestRight:], expr.CallArgs[bestRight+1:])
			copy(expr.CallArgs[bestCenter:], expr.CallArgs[bestCenter+1:])
			copy(expr.CallArgs[bestLeft:], expr.CallArgs[bestLeft+1:])
			expr.CallArgs = expr.CallArgs[:len(expr.CallArgs)-3]

			lift := table.Intern(&Expr{
				Kind:      KindCall,
				Name:      expr.Name,
				CallFlags: expr.CallFlags,
				CallArgs:  []*Expr{best.Left, best.Center, best.Right},
			})

			for i, left := range expr.CallArgs {
				for _, center := range expr.CallArgs[i+1:] {
					list := []*Expr{left, center, lift}
					sortArgs(list)
					key := Partial{expr.Name, list[0], list[1], list[2]}
					count[key]++
				}
			}

			expr.CallArgs = append(expr.CallArgs, lift)
			nextuncollapsed = append(nextuncollapsed, expr)
		}

		uncollapsed, nextuncollapsed = nextuncollapsed, uncollapsed[:0]
	}

	return RemoveUnused(list)
}

type CSE4 struct {
	Eliminated int
}

func (CSE4) Name() string { return "CSE4" }

func (s *CSE4) Run(list []*Expr) []*Expr {
	table := NewTable()
	table.HashAll(list)

	type Partial struct {
		Func  string
		Exprs [4]*Expr
	}

	uncollapsed := Flatten(list, func(e *Expr) bool {
		return e.Kind == KindCall && e.CallFlags == FuncBinary
	})

	nextuncollapsed := []*Expr{}
	for len(uncollapsed) > 0 {
		count := map[Partial]int{}
		for _, expr := range uncollapsed {
			if expr.Kind != KindCall {
				continue
			}
			for i0, a0 := range expr.CallArgs {
				for n1, a1 := range expr.CallArgs[i0+1:] {
					i1 := i0 + 1 + n1
					for n2, a2 := range expr.CallArgs[i1+1:] {
						i2 := i1 + 1 + n2
						for _, a3 := range expr.CallArgs[i2+1:] {
							key := Partial{expr.Name, [4]*Expr{a0, a1, a2, a3}}
							count[key]++
						}
					}
				}
			}
		}

		for _, expr := range uncollapsed {
			if len(expr.CallArgs) <= 4 {
				continue
			}

			highest := 0
			var best Partial
			var bestIndex [4]int

			for i0, a0 := range expr.CallArgs {
				for n1, a1 := range expr.CallArgs[i0+1:] {
					i1 := i0 + 1 + n1
					for n2, a2 := range expr.CallArgs[i1+1:] {
						i2 := i1 + 1 + n2
						for n3, a3 := range expr.CallArgs[i2+1:] {
							i3 := i2 + 1 + n3
							key := Partial{expr.Name, [4]*Expr{a0, a1, a2, a3}}
							if instances := count[key]; instances > highest {
								highest = instances
								best = key
								bestIndex = [4]int{i0, i1, i2, i3}
							}
						}
					}
				}
			}

			if highest <= 1 {
				continue
			}

			s.Eliminated++

			copy(expr.CallArgs[bestIndex[3]:], expr.CallArgs[bestIndex[3]+1:])
			copy(expr.CallArgs[bestIndex[2]:], expr.CallArgs[bestIndex[2]+1:])
			copy(expr.CallArgs[bestIndex[1]:], expr.CallArgs[bestIndex[1]+1:])
			copy(expr.CallArgs[bestIndex[0]:], expr.CallArgs[bestIndex[0]+1:])
			expr.CallArgs = expr.CallArgs[:len(expr.CallArgs)-4]

			lift := table.Intern(&Expr{
				Kind:      KindCall,
				Name:      expr.Name,
				CallFlags: expr.CallFlags,
				CallArgs:  best.Exprs[:],
			})

			for i0, a0 := range expr.CallArgs {
				for n1, a1 := range expr.CallArgs[i0+1:] {
					i1 := i0 + 1 + n1
					for _, a2 := range expr.CallArgs[i1+1:] {
						list := []*Expr{a0, a1, a2, lift}
						sortArgs(list)
						key := Partial{expr.Name, [4]*Expr{list[0], list[1], list[2], list[3]}}
						count[key]++
					}
				}
			}

			expr.CallArgs = append(expr.CallArgs, lift)
			nextuncollapsed = append(nextuncollapsed, expr)
		}

		uncollapsed, nextuncollapsed = nextuncollapsed, uncollapsed[:0]
	}

	return RemoveUnused(list)
}

type CSE5 struct {
	Eliminated int
}

func (CSE5) Name() string { return "CSE5" }

func (s *CSE5) Run(list []*Expr) []*Expr {
	const N = 5
	table := NewTable()
	table.HashAll(list)

	type Partial struct {
		Func  string
		Exprs [N]*Expr
	}

	uncollapsed := Flatten(list, func(e *Expr) bool {
		return e.Kind == KindCall && e.CallFlags == FuncBinary
	})

	nextuncollapsed := []*Expr{}
	for len(uncollapsed) > 0 {
		count := map[Partial]int{}

		for _, expr := range uncollapsed {
			if expr.Kind != KindCall {
				continue
			}

			var includeSubset func(depth int, exprs *[N]*Expr, offset int)
			includeSubset = func(depth int, exprs *[N]*Expr, offset int) {
				if depth == N {
					count[Partial{expr.Name, *exprs}]++
					return
				}
				for i, e := range expr.CallArgs[offset:] {
					exprs[depth] = e
					includeSubset(depth+1, exprs, offset+i+1)
				}
			}

			includeSubset(0, &[N]*Expr{}, 0)
		}

		for _, expr := range uncollapsed {
			if len(expr.CallArgs) <= N {
				continue
			}

			highest := 0
			var best Partial
			var bestIndex [N]int

			var findBest func(depth int, exprs *[N]*Expr, index *[N]int, offset int)
			findBest = func(depth int, exprs *[N]*Expr, index *[N]int, offset int) {
				if depth == N {
					key := Partial{expr.Name, *exprs}
					if instances := count[key]; instances > highest {
						highest = instances
						best = key
						bestIndex = *index
					}
					return
				}
				for i, e := range expr.CallArgs[offset:] {
					exprs[depth] = e
					index[depth] = offset + i
					findBest(depth+1, exprs, index, offset+i+1)
				}
			}
			findBest(0, &[N]*Expr{}, &[N]int{}, 0)

			if highest <= 1 {
				continue
			}

			s.Eliminated++

			for i := N - 1; i >= 0; i-- {
				copy(expr.CallArgs[bestIndex[i]:], expr.CallArgs[bestIndex[i]+1:])
			}
			expr.CallArgs = expr.CallArgs[:len(expr.CallArgs)-N]

			lift := table.Intern(&Expr{
				Kind:      KindCall,
				Name:      expr.Name,
				CallFlags: expr.CallFlags,
				CallArgs:  best.Exprs[:],
			})

			var addToCount func(depth int, exprs *[N]*Expr, index *[N]int, offset int)
			addToCount = func(depth int, exprs *[N]*Expr, index *[N]int, offset int) {
				if depth == N-1 {
					exprs[depth] = lift
					list := *exprs
					sortArgs(list[:])
					key := Partial{expr.Name, list}
					count[key]++
					return
				}
				for i, e := range expr.CallArgs[offset:] {
					exprs[depth] = e
					index[depth] = offset + i
					addToCount(depth+1, exprs, index, offset+i+1)
				}
			}
			addToCount(0, &[N]*Expr{}, &[N]int{}, 0)

			expr.CallArgs = append(expr.CallArgs, lift)
			nextuncollapsed = append(nextuncollapsed, expr)
		}

		uncollapsed, nextuncollapsed = nextuncollapsed, uncollapsed[:0]
	}

	return RemoveUnused(list)
}

type CSE6 struct {
	Eliminated int
}

func (CSE6) Name() string { return "CSE6" }

func (s *CSE6) Run(list []*Expr) []*Expr {
	const N = 6
	table := NewTable()
	table.HashAll(list)

	type Partial struct {
		Func  string
		Exprs [N]*Expr
	}

	uncollapsed := Flatten(list, func(e *Expr) bool {
		return e.Kind == KindCall && e.CallFlags == FuncBinary
	})

	nextuncollapsed := []*Expr{}
	for len(uncollapsed) > 0 {
		count := map[Partial]int{}

		for _, expr := range uncollapsed {
			if expr.Kind != KindCall {
				continue
			}

			var includeSubset func(depth int, exprs *[N]*Expr, offset int)
			includeSubset = func(depth int, exprs *[N]*Expr, offset int) {
				if depth == N {
					count[Partial{expr.Name, *exprs}]++
					return
				}
				for i, e := range expr.CallArgs[offset:] {
					exprs[depth] = e
					includeSubset(depth+1, exprs, offset+i+1)
				}
			}

			includeSubset(0, &[N]*Expr{}, 0)
		}

		for _, expr := range uncollapsed {
			if len(expr.CallArgs) <= N {
				continue
			}

			highest := 0
			var best Partial
			var bestIndex [N]int

			var findBest func(depth int, exprs *[N]*Expr, index *[N]int, offset int)
			findBest = func(depth int, exprs *[N]*Expr, index *[N]int, offset int) {
				if depth == N {
					key := Partial{expr.Name, *exprs}
					if instances := count[key]; instances > highest {
						highest = instances
						best = key
						bestIndex = *index
					}
					return
				}
				for i, e := range expr.CallArgs[offset:] {
					exprs[depth] = e
					index[depth] = offset + i
					findBest(depth+1, exprs, index, offset+i+1)
				}
			}
			findBest(0, &[N]*Expr{}, &[N]int{}, 0)

			if highest <= 1 {
				continue
			}

			s.Eliminated++

			for i := N - 1; i >= 0; i-- {
				copy(expr.CallArgs[bestIndex[i]:], expr.CallArgs[bestIndex[i]+1:])
			}
			expr.CallArgs = expr.CallArgs[:len(expr.CallArgs)-N]

			lift := table.Intern(&Expr{
				Kind:      KindCall,
				Name:      expr.Name,
				CallFlags: expr.CallFlags,
				CallArgs:  best.Exprs[:],
			})

			var addToCount func(depth int, exprs *[N]*Expr, index *[N]int, offset int)
			addToCount = func(depth int, exprs *[N]*Expr, index *[N]int, offset int) {
				if depth == N-1 {
					exprs[depth] = lift
					list := *exprs
					sortArgs(list[:])
					key := Partial{expr.Name, list}
					count[key]++
					return
				}
				for i, e := range expr.CallArgs[offset:] {
					exprs[depth] = e
					index[depth] = offset + i
					addToCount(depth+1, exprs, index, offset+i+1)
				}
			}
			addToCount(0, &[N]*Expr{}, &[N]int{}, 0)

			expr.CallArgs = append(expr.CallArgs, lift)
			nextuncollapsed = append(nextuncollapsed, expr)
		}

		uncollapsed, nextuncollapsed = nextuncollapsed, uncollapsed[:0]
	}

	return RemoveUnused(list)
}
