package ssa

import (
	"sort"
	"unsafe"
)

type InternSort struct {
	Intern
	InlineSingle
	SortArguments
}

func (InternSort) Name() string { return "InternSort" }
func (s *InternSort) Run(list []*Expr) []*Expr {
	for {
		interned := s.Intern.Interned
		inlined := s.InlineSingle.Inlined
		sorted := s.SortArguments.Sorted

		list = s.Intern.Run(list)
		list = s.InlineSingle.Run(list)
		list = s.SortArguments.Run(list)

		if interned == s.Intern.Interned && inlined == s.InlineSingle.Inlined && sorted == s.SortArguments.Sorted {
			return list
		}
	}
}

type Intern struct {
	Interned int
}

func (Intern) Name() string { return "Intern" }

// Intern removes duplicate expressions
func (s *Intern) Run(list []*Expr) []*Expr {
	result := make([]*Expr, 0, len(list))

	table := NewTable()
	for _, expr := range list {
		n := table.Intern(expr)
		if n != expr {
			s.Interned++
			// already included in the list
			continue
		}

		ivalue := table.Intern(expr.Value)
		if ivalue != expr.Value {
			s.Interned++
		}
		expr.Value = ivalue

		changed := false
		for i, arg := range expr.CallArgs {
			iarg := table.Intern(arg)
			if arg != iarg {
				s.Interned++
				changed = true
			}
			expr.CallArgs[i] = iarg
		}

		changed = changed || sortArgs(expr.CallArgs)
		if changed {
			table.Erase(expr)
		}

		result = append(result, expr)
	}

	return RemoveUnused(result)
}

type SortArguments struct {
	Sorted int
}

func (SortArguments) Name() string { return "SortArguments" }

// SortArguments sorts call arguments
func (s *SortArguments) Run(list []*Expr) []*Expr {
	for _, expr := range list {
		if sortArgs(expr.CallArgs) {
			s.Sorted++
		}
	}
	return RemoveUnused(list)
}

func sortArgs(args []*Expr) bool {
	compare := func(i, k int) bool {
		a, b := args[i], args[k]
		return uintptr(unsafe.Pointer(a)) < uintptr(unsafe.Pointer(b))
	}
	if sort.SliceIsSorted(args, compare) {
		return false
	}
	sort.Slice(args, compare)
	return true
}

func removeDupsFromSorted(args []*Expr) []*Expr {
	var last *Expr
	r := args[:0]
	for _, arg := range args {
		if arg != last {
			r = append(r, arg)
			last = arg
		}
	}
	return r
}

type InlineSingle struct {
	Inlined int
}

func (InlineSingle) Name() string { return "InlineSingle" }

func (s *InlineSingle) Run(list []*Expr) []*Expr {
	refCount := map[*Expr]int{}
	for _, expr := range list {
		for _, arg := range expr.CallArgs {
			refCount[arg]++
		}
	}

	for _, expr := range list {
		changed := false
		for i := len(expr.CallArgs) - 1; i >= 0; i-- {
			arg := expr.CallArgs[i]
			if arg.Kind == KindCall && arg.CallFlags == FuncBinary && expr.Name == arg.Name {
				if refCount[arg] > 1 {
					continue
				}

				tail := append([]*Expr{}, expr.CallArgs[i+1:]...)
				expr.CallArgs = append(expr.CallArgs[:i], arg.CallArgs...)
				expr.CallArgs = append(expr.CallArgs, tail...)

				changed = true
				s.Inlined++
			}
		}

		if changed {
			sortArgs(expr.CallArgs)
		}
	}

	return list
}
