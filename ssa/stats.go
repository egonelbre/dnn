package ssa

import (
	"fmt"
	"sort"
	"strings"
)

type Stats struct {
	Loads  int
	Stores int
	Call   map[string]int

	MaxDistance    int
	MedianDistance int
	Bandwidth      int
}

func (a *Stats) Sub(b *Stats) *Stats {
	r := CalculateStats(nil)
	r.Loads = a.Loads - b.Loads
	r.Stores = a.Stores - b.Stores

	calls := map[string]struct{}{}
	for name := range a.Call {
		calls[name] = struct{}{}
	}
	for name := range b.Call {
		calls[name] = struct{}{}
	}

	for name := range calls {
		r.Call[name] = a.Call[name] - b.Call[name]
	}
	r.MaxDistance = a.MaxDistance - b.MaxDistance
	r.MedianDistance = a.MedianDistance - b.MedianDistance
	r.Bandwidth = a.Bandwidth - b.Bandwidth
	return r
}

func (stats *Stats) String() string {
	var b strings.Builder
	b.WriteString(fmt.Sprintf("Load: %5d ", stats.Loads))
	b.WriteString(fmt.Sprintf("Stor: %5d ", stats.Loads))

	// TODO: add other operators
	for _, call := range []string{"Add", "Mul", "Max"} {
		count := stats.Call[call]
		b.WriteString(fmt.Sprintf("%s: %5d ", call, count))
	}

	b.WriteString(fmt.Sprintf("| Max: %5d ", stats.MaxDistance))
	b.WriteString(fmt.Sprintf("Med: %5d ", stats.MedianDistance))
	b.WriteString(fmt.Sprintf("Band: %5d ", stats.Bandwidth))

	return b.String()
}

func CalculateStats(exprs []*Expr) *Stats {
	stats := &Stats{}
	stats.Call = map[string]int{}
	if len(exprs) == 0 {
		return stats
	}

	list := Flatten(exprs, nil)
	for _, expr := range list {
		switch expr.Kind {
		case KindLoad:
			stats.Loads++
		case KindStore:
			stats.Stores++
		case KindCall:
			stats.Call[expr.Name] += len(expr.CallArgs) - 1
		}
	}

	alive := map[*Expr]int{}
	distances := []int{}
	computationDistance := 0
	for i := len(list) - 1; i >= 0; i-- {
		expr := list[i]

		computationDistance += expr.LocalCost()

		lastReference, expecting := alive[expr]
		if expecting {
			distances = append(distances, computationDistance-lastReference)
			delete(alive, expr)
		}

		for _, arg := range expr.CallArgs {
			if arg.IsConst() {
				continue
			}
			if _, waiting := alive[arg]; !waiting {
				alive[arg] = computationDistance
			}
		}

		if len(alive) > stats.Bandwidth {
			stats.Bandwidth = len(alive)
		}
	}

	sort.Ints(distances)

	stats.MaxDistance = distances[len(distances)-1]
	stats.MedianDistance = distances[len(distances)/2]

	return stats
}
