package ssa

var Operators = map[string]string{
	"Mul": "*",
	"Add": "+",
}

func Const(value float64) *Expr {
	return &Expr{
		Kind:  KindConst,
		Const: value,
	}
}

func Name(name string) *Expr {
	return &Expr{
		Kind: KindName,
		Name: name,
	}
}

func Load(location []int) *Expr {
	return &Expr{
		Kind:     KindLoad,
		Location: append([]int{}, location...),
	}
}

func NamedLoad(name string, location []int) *Expr {
	return &Expr{
		Kind:     KindLoad,
		Name:     name,
		Location: append([]int{}, location...),
	}
}

func Store(location []int, value *Expr) *Expr {
	return &Expr{
		Kind:     KindStore,
		Location: append([]int{}, location...),
		Value:    value,
	}
}

func NamedStore(name string, location []int, value *Expr) *Expr {
	return &Expr{
		Kind:     KindStore,
		Name:     name,
		Location: append([]int{}, location...),
		Value:    value,
	}
}

func Mul(a, b *Expr) *Expr {
	return &Expr{
		Kind:      KindCall,
		Name:      "Mul",
		CallFlags: FuncBinary,
		CallArgs:  []*Expr{a, b},
	}
}

func Add(elements ...*Expr) *Expr {
	return &Expr{
		Kind:      KindCall,
		Name:      "Add",
		CallFlags: FuncBinary,
		CallArgs:  elements,
	}
}

func Call(name string, elements ...*Expr) *Expr {
	return &Expr{
		Kind:      KindCall,
		Name:      name,
		CallFlags: FuncCall,
		CallArgs:  elements,
	}
}
