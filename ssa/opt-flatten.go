package ssa

func FlattenNonConst(roots []*Expr) []*Expr {
	return Flatten(roots, func(e *Expr) bool {
		return !e.IsConst()
	})
}

// Flatten extracts all expressions to a single list
func Flatten(roots []*Expr, match func(e *Expr) bool) []*Expr {
	if match == nil {
		match = func(*Expr) bool { return true }
	}

	list := []*Expr{}
	included := make(map[*Expr]struct{})

	var flatten func(e *Expr)
	flatten = func(e *Expr) {
		if e == nil || !match(e) {
			return
		}
		if _, ok := included[e]; ok {
			return
		}
		included[e] = struct{}{}

		flatten(e.Value)
		for _, arg := range e.CallArgs {
			flatten(arg)
		}

		list = append(list, e)
	}

	for _, root := range roots {
		flatten(root)
	}

	return list
}

func RemoveUnused(roots []*Expr) []*Expr {
	stores := Flatten(roots, func(e *Expr) bool {
		return e.Kind == KindStore
	})
	return Flatten(stores, nil)
}
