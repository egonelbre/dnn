package ssa

type Calc struct {
	Eliminated int
	Optimized  int
}

func (Calc) Name() string { return "Calc" }

// Calc:
//  min(a, 5, 10) => min(a, 5)
//  max(a, 5, 10) => max(a, 10)
//  add(a, 5, 10) => add(a, 15)
//  mul(a, 5, 10) => mul(a, 50)
func (s *Calc) Run(list []*Expr) []*Expr {
	nonconsts, consts := []*Expr{}, []*Expr{}
	for _, e := range FlattenNonConst(list) {
		if !e.IsBinary() {
			continue
		}

		nonconsts = nonconsts[:0]
		consts = consts[:0]
		for _, arg := range e.CallArgs {
			if arg.IsConst() {
				consts = append(consts, arg)
			} else {
				nonconsts = append(nonconsts, arg)
			}
		}

		if len(consts) == 0 {
			continue
		}

		total := consts[0].Const

		switch e.Name {
		case "Min":
			for _, v := range consts[1:] {
				if v.Const < total {
					total = v.Const
				}
			}
		case "Max":
			for _, v := range consts[1:] {
				if v.Const > total {
					total = v.Const
				}
			}
		case "Mul":
			for _, v := range consts[1:] {
				total *= v.Const
			}
		case "Add":
			for _, v := range consts[1:] {
				total += v.Const
			}
		default:
			continue
		}

		if len(nonconsts) == 0 {
			*e = *Const(total)
			s.Eliminated++
		} else {
			e.CallArgs = append(e.CallArgs[:0], nonconsts...)
			e.CallArgs = append(e.CallArgs, Const(total))
			s.Optimized++
		}
	}
	return list
}
