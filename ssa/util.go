package ssa

func UnlinkConsts(exprs []*Expr) []*Expr {
	result := make([]*Expr, 0, len(exprs))

	for _, expr := range Flatten(exprs, nil) {
		if expr.IsConst() {
			continue
		}

		if expr.Value.IsConst() {
			expr.Value = expr.Value.ShallowClone()
		}
		for i, arg := range expr.CallArgs {
			if arg.IsConst() {
				expr.CallArgs[i] = arg.ShallowClone()
			}
		}

		result = append(result, expr)
	}

	return result
}
