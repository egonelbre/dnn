package tensor

import (
	"sort"
	"strconv"
)

type Shape struct {
	Dims []int
	Size int
}

func NewShape(dims ...int) Shape {
	size := 1
	for _, v := range dims {
		size *= v
	}
	return Shape{
		Dims: dims,
		Size: size,
	}
}

func (a Shape) Eq(b Shape) bool {
	if len(a.Dims) != len(b.Dims) {
		return false
	}
	for i := range a.Dims {
		if a.Dims[i] != b.Dims[i] {
			return false
		}
	}
	return true
}

func (a Shape) Index(at []int) int {
	if len(at) != len(a.Dims) {
		panic("shape and coordinate mismatch")
	}

	// TODO: write better loop
	index, factor := 0, 1
	for i := len(a.Dims) - 1; i >= 0; i-- {
		index += factor * at[i]
		factor *= a.Dims[i]
	}

	return index
}

func (a Shape) String() string {
	r := "<"
	for i, v := range a.Dims {
		if i != 0 {
			r += ","
		}
		r += strconv.Itoa(v)
	}
	r += ">"
	return r
}

func (shape *Shape) Iterate(stride []int) *Iterator {
	if len(stride) == 0 {
		stride = make([]int, len(shape.Dims))
	}
	for i, v := range stride {
		if v == 0 {
			stride[i] = 1
		}
	}
	return &Iterator{nil, nil, append([]int{}, shape.Dims...), stride}
}

func (shape *Shape) Squeeze(axes ...int) Shape {
	axes = append([]int{}, axes...)
	if len(axes) == 0 {
		for i, dim := range shape.Dims {
			if dim == 1 {
				axes = append(axes, i)
			}
		}
	}
	sort.Ints(axes)

	dims := append([]int{}, shape.Dims...)
	for i := len(axes) - 1; i >= 0; i-- {
		axis := axes[i]
		if dims[axis] != 1 {
			panic("cannot squeeze " + strconv.Itoa(dims[axis]))
		}
		dims = append(dims[:axis], dims[axis+1:]...)
	}

	return NewShape(dims...)
}

type Iterator struct {
	Location []int
	Zero     []int
	Dims     []int
	Stride   []int
}

func (it *Iterator) Next() bool {
	if len(it.Location) == 0 {
		if len(it.Dims) == 0 {
			return false
		}
		if len(it.Zero) == 0 {
			it.Zero = make([]int, len(it.Dims))
		}
		it.Location = make([]int, len(it.Dims))
		copy(it.Location, it.Zero)
		return true
	}

	last := len(it.Location) - 1
	for axis := last; axis >= 0; axis-- {
		it.Location[axis] += it.Stride[axis]
		if it.Location[axis] < it.Dims[axis] {
			return true
		}
		it.Location[axis] = it.Zero[axis]
	}

	return false
}

func (it *Iterator) Offset(by []int) []int {
	if len(it.Location) != len(by) {
		panic("offset does not match iterator")
	}

	loc := append([]int{}, it.Location...)
	for axis, offset := range by {
		loc[axis] += offset
	}
	return loc
}
