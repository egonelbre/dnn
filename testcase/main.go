// +build ignore

package main

import (
	"flag"
	"fmt"
	"math/rand"

	"gitlab.com/egonelbre/dnn/ssa"
	"gitlab.com/egonelbre/dnn/tensor"
)

func main() {
	flag.Parse()
	rand.Seed(0)

	block := tensor.New(6, 6, 3)
	kernel := tensor.New(3, 3, 3, 64)

	Iota(block)
	Iota(kernel)

	result := block.Convolve(kernel, []int{1, 1, 0})

	calc := ssa.Calc{}
	for i, v := range result.Data {
		r := ssa.Optimize(
			[]*ssa.Expr{v},
			&calc,
		)
		result.Data[i] = r[0]
		fmt.Printf("%.0f\n", r[0].Const)
	}

	fmt.Println(result.Shape, calc)
}

func Iota(t *tensor.Tensor) {
	it := t.Iterate(nil)
	for it.Next() {
		v := 0
		for _, x := range it.Location {
			v += x
		}
		t.Set(it.Location, ssa.Const(float64(v)))
	}
}
