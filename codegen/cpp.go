package codegen

import (
	"fmt"
	"io"
	"strconv"
	"strings"

	"gitlab.com/egonelbre/dnn/ssa"
)

func Cpp(w io.Writer, exprs []*ssa.Expr) error {
	var err error
	write := func(format string, args ...interface{}) {
		if err != nil {
			return
		}
		_, err = fmt.Fprintf(w, format+"\n", args...)
	}

	exprs = ssa.FlattenNonConst(exprs)

	refCount := map[*ssa.Expr]int{}
	for _, expr := range exprs {
		for _, arg := range expr.CallArgs {
			refCount[arg]++
		}
	}

	variableNames := map[*ssa.Expr]string{}

	newName := func(e *ssa.Expr) string {
		i := len(variableNames)
		name := "t" + strconv.Itoa(i)
		variableNames[e] = name
		return name
	}

	formatLocation := func(args []int) string {
		xs := []string{}
		for _, arg := range args {
			xs = append(xs, strconv.Itoa(arg))
		}
		return strings.Join(xs, ", ")
	}

	var variableSymbol func(expr *ssa.Expr) string
	expressionString := func(expr *ssa.Expr) string {
		if expr.IsConst() {
			return expr.String()
		}

		switch expr.Kind {
		case ssa.KindLoad:
			return "LOAD(" + expr.Name + ", " + formatLocation(expr.Location) + ")"
		case ssa.KindStore:
			return "STORE(" + expr.Name + ", " + formatLocation(expr.Location) + ", " + variableSymbol(expr.Value) + ")"
		case ssa.KindCall:
			args := []string{}
			for _, arg := range expr.CallArgs {
				args = append(args, variableSymbol(arg))
			}

			if op, isOp := ssa.Operators[expr.Name]; isOp {
				return "(" + strings.Join(args, " "+op+" ") + ")"
			}

			return expr.Name + "(" + strings.Join(args, ",") + ")"
		default:
			panic("unhandled")
		}
	}

	variableSymbol = func(expr *ssa.Expr) string {
		if expr.IsConst() {
			return expr.String()
		}

		varName := variableNames[expr]
		if varName == "" {
			return expressionString(expr)
		}

		return varName
	}

	for _, expr := range exprs {
		if refCount[expr] <= 1 && expr.Kind != ssa.KindStore {
			continue
		}

		switch expr.Kind {
		case ssa.KindStore:
			write(expressionString(expr))
		case ssa.KindLoad:
			name := newName(expr)
			write("float %v = %v;", name, expressionString(expr))
		case ssa.KindCall:
			name := newName(expr)
			write("float %v = %v;", name, expressionString(expr))
		}
	}

	return nil
}
