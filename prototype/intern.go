package main

import "sort"

type Table struct {
	ID     map[*V]int
	Lookup map[string]*V
}

func NewTable() *Table {
	return &Table{
		ID:     map[*V]int{},
		Lookup: map[string]*V{},
	}
}

func (table *Table) Intern(v *V) *V {
	if _, ok := table.ID[v]; ok {
		return v
	}
	if v.Op == OpConst {
		return v
	}

	for i, arg := range v.Args {
		v.Args[i] = table.Intern(arg)
	}
	if v.Op == OpMul || v.Op == OpAdd {
		k := 0
		for _, arg := range v.Args {
			if !arg.IsZero() {
				v.Args[k] = arg
				k++
			}
		}

		v.Args = v.Args[:k]
		if len(v.Args) == 0 {
			return table.Intern(Const(0))
		}
	}

	sort.Slice(v.Args, func(i, k int) bool {
		return table.ID[v.Args[i]] < table.ID[v.Args[k]]
	})

	if v.Op == OpMul {
		for _, arg := range v.Args {
			if arg.IsZero() {
				return table.Intern(Const(0))
			}
		}
	}

	s := v.String()
	collapsed, ok := table.Lookup[s]
	if ok {
		return collapsed
	}

	table.ID[v] = len(table.Lookup)
	table.Lookup[s] = v
	return v
}
