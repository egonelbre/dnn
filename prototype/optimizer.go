package main

import (
	"sort"
)

func BinaryReduce1(partials []*V) {
	table := NewTable()

	for _, partial := range partials {
		for _, arg := range partial.Args {
			table.Intern(arg)
		}
		sort.Slice(partial.Args, func(i, k int) bool {
			return table.ID[partial.Args[i]] < table.ID[partial.Args[k]]
		})
	}

	paircount := map[[2]*V]int{}
	uncollapsed := append(partials[:0:0], partials...)

	for _, partial := range uncollapsed {
		for i, left := range partial.Args {
			for _, right := range partial.Args[i+1:] {
				pair := [2]*V{left, right}
				paircount[pair]++
			}
		}
	}

	nextuncollapsed := []*V{}
	for len(uncollapsed) > 0 {
		for _, partial := range uncollapsed {
			highest := 0
			var bestpair [2]*V
			var bestLeft, bestRight int

			for i, left := range partial.Args {
				for k, right := range partial.Args[i+1:] {
					pair := [2]*V{left, right}
					count := paircount[pair]
					if count > highest {
						highest = count
						bestpair = pair
						bestLeft, bestRight = i, k
					}
				}
			}

			copy(partial.Args[bestRight:], partial.Args[bestRight+1:])
			copy(partial.Args[bestLeft:], partial.Args[bestLeft+1:])

			result := table.Intern(&V{Op: partial.Op, Args: bestpair[:]})

			for _, left := range partial.Args {
				pair := [2]*V{left, result}
				paircount[pair]++
			}

			partial.Args = append(partial.Args[:len(partial.Args)-2], result)

			if len(partial.Args) > 2 {
				nextuncollapsed = append(nextuncollapsed, partial)
				continue
			}

			if len(partial.Args) == 2 {
				partial.Args = []*V{
					table.Intern(&V{Op: partial.Op, Args: bestpair[:]}),
				}
			}
		}

		uncollapsed, nextuncollapsed = nextuncollapsed, uncollapsed[:0]
	}
}
